import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

const Choice = (props) => {
  return (
    <FontAwesomeIcon icon={props.icon} className={`pr-1 cursor-pointer color:blue ${props.classes}`}/>
  )
}

export default Choice;
