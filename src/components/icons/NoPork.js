import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faBacon, faBan} from "@fortawesome/free-solid-svg-icons";

const NoPork = () => {
  return (
    <span className="fa-layers fa-fw  mr-1" title="Ingen gris">
    <FontAwesomeIcon icon={faBacon} transform="grow-6" title="Ingen svinekød"/>
    <FontAwesomeIcon icon={faBan} color="red"/>
  </span>
  )
}

export default NoPork;
