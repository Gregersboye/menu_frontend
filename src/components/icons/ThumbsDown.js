import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faCircleArrowDown} from '@fortawesome/free-solid-svg-icons';

const ThumbsDown = ({titleText}) => {
  return (
    <FontAwesomeIcon className="hover:text-cyan-200 hover:cursor-pointer fa-flip-horizontal" icon={faCircleArrowDown}  transform="grow-6" title={titleText}/>
  )
}

export default ThumbsDown;
