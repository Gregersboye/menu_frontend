import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faCircleArrowUp} from "@fortawesome/free-solid-svg-icons";

const ThumbsUp = ({ titleText}) => {
  return (
    <FontAwesomeIcon className="hover:text-cyan-200 hover:cursor-pointer" icon={faCircleArrowUp}  transform="grow-6" title={titleText}/>
  )
}

export default ThumbsUp;
